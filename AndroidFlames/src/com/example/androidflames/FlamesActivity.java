package com.example.androidflames;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FlamesActivity extends Activity implements OnClickListener {
	EditText firstName, lastName;
	TextView output;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		firstName = (EditText) findViewById(R.id.first);
		lastName = (EditText) findViewById(R.id.second);
		Button button = (Button) findViewById(R.id.button1);
		output = (TextView) findViewById(R.id.textView3);
		button.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		if (R.id.button1 == v.getId()) {
			if (firstName.getText().toString().isEmpty()) {

				Toast.makeText(getApplicationContext(),
						"You Could not enter First Name Please Enter Name",
						Toast.LENGTH_LONG).show();
			} else if (lastName.getText().toString().isEmpty()) {
				Toast.makeText(getApplicationContext(),
						"You Could not enter Last Name Please Enter Name",
						Toast.LENGTH_LONG).show();
			} else {
				String str1 = firstName.getText().toString();
				String str2 = lastName.getText().toString();
				if (str1.equalsIgnoreCase(str2)) {
					Toast.makeText(
							getApplicationContext(),
							"FLAME works only for different names. You have entered the same name for both persons.",
							Toast.LENGTH_LONG).show();
				} else {
					int count = 0;
					String result = "";
					int k = 0;
					char first[] = str1.toLowerCase().toCharArray();
					char second[] = str2.toLowerCase().toCharArray();
					for (int i = 0; i < str1.length(); i++) {
						count = 0;
						for (int j = 0; j < str2.length(); j++) {

							if (first[i] == second[j]) {
								first[i] = '*';
								second[j] = '*';
								count++;

								if (count == 1) {
									k++;
								}
							}
						}
						int value = str1.length() + str2.length() - (2 * k);
						StringBuffer str = new StringBuffer("FLAMES");
						int g = 0;
						while (str.length() > 1) {
							g = (g + value - 1) % str.length();
							result = str.deleteCharAt(g).toString();
						}

					}

					output.setText("                                  "
							+ result);
				}
			}

		}
	}
}
